import gulp from 'gulp';
import php2html from 'gulp-php2html';
import config from './config';

const phpToHtml = () => gulp.src(`${config.build}/**/*.php`)
  .pipe(php2html())
  .pipe(gulp.dest(`${config.build}/html/`));

export { phpToHtml };
