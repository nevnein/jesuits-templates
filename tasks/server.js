import gulp from 'gulp';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';

import { config as webpackConfig } from './scripts';
import { browser } from './browser';
import { scss } from './scss';
import { templates } from './templates';
import config from './config';

const bundler = webpack(webpackConfig);

function server() {
  const baseBrowserConfig = {
    middleware: [
      webpackDevMiddleware(bundler, { /* options */ }),
    ],
  };

  const browserConfig = config.proxy
    ? Object.assign(baseBrowserConfig, {
      proxy: config.proxy,
    })
    : Object.assign(baseBrowserConfig, {
      server: {
        baseDir: config.src,
        index: config.entry,
      },
    });

  browser.init(browserConfig);

  gulp.watch('assets/scss/**/*.scss', { cwd: config.src }, gulp.parallel(scss));

  gulp.watch('assets/js/**/*.js', { cwd: config.src }, (done) => {
    browser.reload();
    done();
  });

  // gulp.watch('*.html', { cwd: config.src }, (done) => {
  //   browser.reload();
  //   done();
  // });

  gulp.watch(['templates/**/*.njk', 'templates/_data.json'], { cwd: config.src }, gulp.series(templates, (done) => {
    browser.reload();
    done();
  }));

  gulp.watch('**/*.php', { cwd: config.src }, (done) => {
    browser.reload();
    done();
  });
}

export { server };
