import config from './config';

const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, `../${config.src}/assets/js/main.js`),
  output: {
    filename: '[name].dist.js',
    path: path.resolve(__dirname, `../${config.src}/assets/js`),
    library: 'jesuitsTemplates',
    libraryTarget: 'window',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/,
        options: {
          presets: [
            ['@babel/preset-env', {
              useBuiltIns: 'usage',
              corejs: 3,
            }],
          ],
          babelrc: false,
        },
      },
    ],
  },
  mode: 'development',
  watch: true,
};
