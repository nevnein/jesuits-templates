import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import cleanCSS from 'gulp-clean-css';
import gulpIf from 'gulp-if';

import { browser } from './browser';
import config from './config';

const scss = () => gulp.src('assets/scss/*.scss', { cwd: config.src })
  .pipe(gulpIf(config.env === 'development', sourcemaps.init()))
  .pipe(sass(config.sass).on('error', sass.logError))
  .pipe(autoprefixer({ cascade: false }))
  .pipe(cleanCSS())
  .pipe(gulpIf(config.env === 'development', sourcemaps.write('.')))
  .pipe(gulp.dest('assets/css', { cwd: config.src }))
  .pipe(browser.stream());

export { scss };
