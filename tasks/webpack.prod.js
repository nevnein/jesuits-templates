import config from './config';

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const path = require('path');

// Avoid naming conflicts
const suffix = config.src === config.build
  ? 'build'
  : 'dist';

module.exports = {
  entry: path.resolve(__dirname, `../${config.src}/assets/js/main.js`),
  output: {
    filename: `[name].${suffix}.js`,
    path: path.resolve(__dirname, `../${config.build}/assets/js`),
    library: 'jesuitsTemplates',
    libraryTarget: 'window',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/,
        options: {
          presets: [
            ['@babel/preset-env', {
              useBuiltIns: 'usage',
              debug: config.debug,
              corejs: 3,
            }],
          ],
          plugins: [
            '@babel/plugin-transform-runtime',
          ],
          babelrc: false,
        },
      },
    ],
  },
  mode: 'production',
  plugins: [],
};

if (config.debug) {
  module.exports.plugins.push(new BundleAnalyzerPlugin());
}
