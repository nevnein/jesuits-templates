import gulp from 'gulp';
import log from 'fancy-log';

import config from './config';
import { scripts } from './scripts';
import { server } from './server';
import { scss } from './scss';
import { build } from './build';
import { images } from './images';
import { phpToHtml } from './phpToHtml';
import { templates, templatesCleanup } from './templates';

const watch = gulp.series(
  templatesCleanup,
  gulp.parallel(scss, scripts, templates),
  server,
);

// Debugger task, log anything
gulp.task('log', (done) => {
  log(config.src === config.build);
  done();
});

export {
  scss,
  scripts,
  server,
  build,
  images,
  phpToHtml,
  templates,
  templatesCleanup
};

/* DEFAULT */
export default watch;
