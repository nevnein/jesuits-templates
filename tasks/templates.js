import gulp from 'gulp';
import del from 'del';
import nunjucksRender from 'gulp-nunjucks-render';

import config from './config';

function requireUncached(module) {
  delete require.cache[require.resolve(module)];
  return require(module);
}

const templates = () => gulp.src(['templates/**/*.njk', '!templates/**/_*.njk'], { cwd: config.src })
  .pipe(nunjucksRender({
    ext: '.html',
    path: 'src/templates',
    data: requireUncached('./../src/templates/_data.json'),
  }))
  .pipe(gulp.dest('.', { cwd: config.src }))

const templatesCleanup = async () => await del(`${config.src}/*.html`);

export { templates, templatesCleanup };
