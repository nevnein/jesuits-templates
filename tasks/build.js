const gulp = require('gulp');
const del = require('del');

import config from './config';
import { images } from './images';
import { scripts } from './scripts';
import { scss } from './scss';
import { templates } from './templates';

// Minitask to clean the build folder (Avoid deleting base folder PLS)
const clean = () => (
  config.build !== './' &&  config.build !== '.'
    ? del(`${config.build}/**/*`)
    : null
);

// Files to move TODO: move to config;
const filesToMove = [
  `${config.src}/assets/css/*.css`,
  `${config.src}/assets/fonts/*.*`,
  `${config.src}/**/*.php`,
  `${config.src}/*`,
  `!${config.src}/templates`,
];

// Minitask to move files to the build folder
const relocate = (done) => {
  gulp.src(filesToMove, { base: config.src })
    .pipe(gulp.dest(`${config.build}/`));
  done();
};

// Clean the build folder, optimize assets, move everything in the build folder
const build = config.src !== config.build
  ? gulp.series(
    clean,
    gulp.parallel(images, scripts, scss, templates),
    relocate,
  )
  : gulp.parallel(scripts, scss);

export { build };
