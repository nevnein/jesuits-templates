let config = {};
const env = process.env.NODE_ENV || 'development';
const debug = process.env.DEBUG === 'true';
const tildeImporter = require('node-sass-tilde-importer');

try {
  // eslint-disable-next-line
  config = require('../freckles.config.json');
} catch (err) {
  console.log('\x1b[30m\x1b[43m --- WARNING --- \x1b[0m No Freckles config file found, using defaults');
}

const defaultConfig = {
  env,
  debug,
  name: 'freckles',
  src: './',
  build: './',
  proxy: false,
  entry: 'index.html',
  sass: {
    importer: tildeImporter,
  }
};

const mainConfig = Object.assign(defaultConfig, config);

export default mainConfig;
