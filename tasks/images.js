import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import cache from 'gulp-cached';
import config from './config';

// TODO: move imagemin config to main config, test task with all type of images
const images = () => gulp.src(`${config.src}/assets/images/**/*`)
  .pipe(cache('images-cache'))
  .pipe(imagemin([
    imagemin.gifsicle({ interlaced: true }),
    imagemin.mozjpeg({ progressive: true }),
    imagemin.optipng({ optimizationLevel: 5 }),
    imagemin.svgo({
      plugins: [
        { removeViewBox: false },
        { cleanupIDs: false },
      ],
    }),
  ], {
    verbose: true,
  }))
  .pipe(gulp.dest(`${config.build}/assets/images`));

export { images };
