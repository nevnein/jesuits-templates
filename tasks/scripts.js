import webpack from 'webpack';
import mainConfig from './config';

// TODO fix webpack configurations
const prodConfig = require('./webpack.prod');
const devConfig = require('./webpack.dev');

const config = mainConfig.env === 'production' ? prodConfig : devConfig;

function scripts() {
  return new Promise(resolve => webpack(config, (err, stats) => {
    if (err) console.log('Webpack', err);
    if (mainConfig.env === 'production') console.log(stats.toString({ colors: true }));
    resolve();
  }));
}

export { config, scripts };
