# 5A Freckles
A heavily opinionated framework by 5A Design.

## Features
- Livereloading and code injecting
- ES6 support with Babel transpiling and polyfilling, supports IE10 & > 
- SCSS compiler
- Assets and images optimization
- JS and SCSS code linting

## Installation
1. Clone this repo
2. Remove this repo and initialize a new project `rm -rf .git && git init`
3. Modify the `package.json` with your desired config
4. Install dependencies `yarn install`

## Use
- `yarn start` to boot up the development server
- `yarn build` to compile the production files in the `build/` folder

## Code linting
On VsCode, install [Stylelint](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint) for CSS and SCSS linting and [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) for JS linting.

The other required files and configuration are already included in this repo, feel free to change the configs.
___

![Gidio!](https://5adesign.it/assets/img/gidio.gif "Gidio!") That's all folks, happy coding!