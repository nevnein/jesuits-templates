import Flickity from 'flickity';
import 'flickity-imagesloaded';

Array.from(document.querySelectorAll('.carousel__slider')).forEach(el => new Flickity(el, {
  cellAlign: 'left',
  wrapAround: true,
  imagesLoaded: true,
}));

window.Flickity = Flickity;
